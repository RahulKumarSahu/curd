<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <style>
      h2{
          text-align: center;
          text-shadow: -2px 1px greenyellow;
          font-family:Verdana, Geneva, Tahoma, sans-serif;
      }
  </style>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-light">
        <ul class="navbar-nav">
          <li class="nav-item">
          </li>
          <li class="nav-item">
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"></a>
          </li>
        </ul>
      </nav>

<div class="container-all">
  <div class="container">
      <h2>CURD OPERATION</h2>
    <a href="{{ route('create') }}"> <button type="button" class="btn btn-outline-primary">Insert page</button></a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Class</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

            @foreach ($users as $user)

            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->gender }}</td>
                <td>{{ $user->class }}</td>

                <td> <span class="badge badge-{{ $user->status== 'active'?'success':'danger' }}">{{ $user->status }}</span>
                </td>

                <td>
                    <a href="{{ route('show',$user->id) }}"><button type="submit" class="btn btn-warning">Show</button></a>
                    <a href="{{ route('edit',$user->id) }}"><button type="submit" class="btn btn-success">Edit</button></a>

                    <form action="{{ route('destroy', $user->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>

                </td>
            </tr>

            @endforeach

        </tbody>
      </table>
    </div>

</div>

</body>
</html>
